﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Windows.Forms;
using System.IO;
namespace RounghSetTheory
{
    public class WorkInXML
    {
        public VoidEvents CallLoadXML;
        public MainEvents CallEditXML;
        public MainEvents CallAddInXML;

        public  WorkInXML()
        {
            CallLoadXML = new VoidEvents(LoadFromXML);
            CallEditXML = new MainEvents(EditIncidentInXML);
            CallAddInXML = new MainEvents(AddNewIncident);
        }

        
        XmlDocument incidentsXML = new XmlDocument();

        List<TypeIncident> typeIncidents = new List<TypeIncident>();

        public List<TypeIncident> GetIncidentsList()
        {

            MessageBox.Show(typeIncidents[0].incidents[0].classes.Count.ToString());
            return typeIncidents;
        }



        /// <summary>
        /// загружает данные об инцидентах из XML
        /// </summary>
        void LoadFromXML()
        {

            var incident = ("", "", new float[2], new List<List<float>>(), new Dictionary<string, string>(), "", "");
            Dictionary<string, string> tempD = new Dictionary<string, string>();
            incidentsXML.Load("Incidents.xml");



            XmlElement root = incidentsXML.DocumentElement;

            foreach (XmlNode node in root)
            {
                TypeIncident typeIncident = new TypeIncident();
                XmlNode attribute = node.Attributes.GetNamedItem("name");
                typeIncident.NameTypeIncident = attribute.Value;


                foreach (XmlNode child in node.ChildNodes)
                {
                    if (child.Name == "nameIncident")
                        incident.Item1 = child.InnerText;
                    if (child.Name == "description")
                        incident.Item2 = child.InnerText;
                    if (child.Name == "range")
                    {
                        incident.Item3 = ParseRange(child.InnerText);
                    }
                    if (child.Name == "array")
                    {
                        incident.Item4 = ParserArray(child.InnerText);
                    }

                    if (child.Name == "class1")
                    {
                        incident.Item5 = new Dictionary<string, string>
                        {
                            { child.InnerText, "no" }
                        };
                    }
                    
                    if (child.Name == "class2")
                    {
                        incident.Item5.Add(child.InnerText, "no");
                    }

                    if (child.Name == "class3")
                    {                        
                        incident.Item5.Add(child.InnerText, "no");
                    }


                    if (child.Name == "solution")
                    {
                        incident.Item6 = child.InnerText;

                        
                    }

                    if (child.Name == "date")
                    {
                        incident.Item7 = child.InnerText;

                        Incident inc = new Incident(incident);

                        typeIncident.incidents.Add(inc);
                    }
                }

                
                typeIncidents.Add(typeIncident);

                
            }
            
        }


        /// <summary>
        /// Парсит альфу и бетту диапазона
        /// </summary>
       float[] ParseRange(string buffer)
       {
            float[] temp = new float[2];
            int indexArray = 0;
            string numbBuffer = "";

            for (int i = 0; i < buffer.Length; i++)
            {
                if (buffer[i] == ' ')
                    continue;

                if (buffer[i] == '.')
                {
                    try
                    {
                        temp[indexArray] = float.Parse(numbBuffer);
                       
                        numbBuffer = "";
                        indexArray++;
                    }
                    catch { MessageBox.Show("Error in XML, Edit ALPHA or BETTA"); }

                    continue;
                }

                
                numbBuffer += buffer[i];


                if (i+1 == buffer.Length)
                {
                    try
                    {
                        temp[indexArray] = float.Parse(numbBuffer);
                    }
                    catch { MessageBox.Show("Error in XML, Edit ALPHA or BETTA"); }
                }                          
            }
            return temp;
       }


        /// <summary>
        /// Парсит матрицу измерений
        /// </summary>
       List<List<float>> ParserArray (string buffer)
        {
            string tempBuf = "";
            List<List<float>> tempList = new List<List<float>>
            {
                new List<float>()
            };

            int indexList = 0;

            for (int i = 0; i < buffer.Length; i++)
            {
                if (buffer[i] == ' ')
                {
                    try
                    {

                        tempList[indexList].Add(float.Parse(tempBuf));
                    }
                    catch
                    {
                        MessageBox.Show("Error in XML, Edit Array Atributes");
                    }

                    tempBuf = "";
                    continue;
                }

                if (buffer[i] == '.')
                {
                    try
                    {

                        tempList[indexList].Add(float.Parse(tempBuf));
                    }
                    catch
                    {
                        MessageBox.Show("Error in XML, Edit Array Atributes");
                    }

                    tempBuf = "";

                    tempList.Add(new List<float>());
                    indexList++;

                    continue;
                }

                tempBuf += buffer[i];

                if (i + 1 == buffer.Length)
                {
                    try
                    {
                        tempList[indexList].Add(float.Parse(tempBuf));
                    }
                    catch
                    {
                        MessageBox.Show("error in xml, edit array atributes");
                    }
                }
            }


           
            return tempList;

        }

        void EditIncidentInXML(SubContainer helper)
        {

            int internalIndex = 0;

            incidentsXML.Load("Incidents.xml");



            XmlElement root = incidentsXML.DocumentElement;

            foreach (XmlNode node in root)
            {              
                XmlNode attribute = node.Attributes.GetNamedItem("name");

                if (attribute.Value == helper.nameTypeIncident)
                {
                    foreach (XmlNode child in node.ChildNodes)
                    {
                        if (internalIndex == helper.editIndex && child.Name == "nameIncident") child.InnerText = helper.nameIncident;

                        if (internalIndex == helper.editIndex && child.Name == "description")
                        {
                            child.InnerText = helper.description;
                            internalIndex++;
                            break;
                        }

                        if (internalIndex < helper.editIndex && child.Name == "array") internalIndex++;
                    }

                    if (internalIndex > helper.editIndex)
                        break;
                }
            }

            incidentsXML.Save("Incidents.xml");

        }

        void AddNewIncident(SubContainer helper)
        {
            incidentsXML.Load("Incidents.xml");

            XmlElement root = incidentsXML.DocumentElement;

            foreach (XmlNode node in root)
            {
                XmlNode attribute = node.Attributes.GetNamedItem("name");

                if (attribute.Value == helper.nameTypeIncident)
                {
                    XmlElement incident = incidentsXML.CreateElement("nameIncident");
                    XmlElement description = incidentsXML.CreateElement("description");
                    XmlElement class1 = incidentsXML.CreateElement("class1");
                    XmlElement class2 = incidentsXML.CreateElement("class2");
                    XmlElement class3 = incidentsXML.CreateElement("class3");
                    XmlElement range = incidentsXML.CreateElement("range");
                    XmlElement array = incidentsXML.CreateElement("array");
                    XmlElement solution = incidentsXML.CreateElement("solution");
                    XmlElement date = incidentsXML.CreateElement("date");

                    XmlText incidentText = incidentsXML.CreateTextNode(helper.nameIncident);
                    XmlText descriptionText = incidentsXML.CreateTextNode(helper.description);

                    XmlText class1Text = incidentsXML.CreateTextNode(helper.classes["class1"]);
                    XmlText class2Text = incidentsXML.CreateTextNode(helper.classes["class2"]);
                    XmlText class3Text = incidentsXML.CreateTextNode(helper.classes["class3"]);
                    XmlText rangeText = incidentsXML.CreateTextNode(helper.alpha.ToString() + "," + helper.betta.ToString());
                    

                    string matrixBuf= "";

                    foreach (var value in helper.matrix)
                    {
                        for (var element = 0; element < value.Count; element++)
                        {
                            if (element < value.Count-1)
                                matrixBuf += value[element] + " ";
                            else
                                matrixBuf += value[element];
                        }
                        matrixBuf += ".";
                    }

                    XmlText arrayText = incidentsXML.CreateTextNode(matrixBuf);
                    XmlText solutionText = incidentsXML.CreateTextNode(helper.solution);
                    XmlText dateText = incidentsXML.CreateTextNode(helper.date);

                    incident.AppendChild(incidentText);
                    description.AppendChild(descriptionText);
                    class1.AppendChild(class1Text);
                    class2.AppendChild(class2Text);
                    class3.AppendChild(class3Text);
                    range.AppendChild(rangeText);
                    array.AppendChild(arrayText);
                    solution.AppendChild(solutionText);
                    date.AppendChild(dateText);

                    node.AppendChild(incident);
                    node.AppendChild(description);
                    node.AppendChild(class1);
                    node.AppendChild(class2);
                    node.AppendChild(class3);
                    node.AppendChild(range);
                    node.AppendChild(array);
                    node.AppendChild(solution);
                    node.AppendChild(date);
                }
            }

            incidentsXML.Save("Incidents.xml");

        }

    }

                
}
 


