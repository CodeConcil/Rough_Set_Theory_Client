﻿namespace RounghSetTheory
{
    partial class EditIncident
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EditDescriptionTextBox = new System.Windows.Forms.RichTextBox();
            this.EditNameIncidentTextBox = new System.Windows.Forms.TextBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.numberIncLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // EditDescriptionTextBox
            // 
            this.EditDescriptionTextBox.Location = new System.Drawing.Point(46, 71);
            this.EditDescriptionTextBox.Name = "EditDescriptionTextBox";
            this.EditDescriptionTextBox.Size = new System.Drawing.Size(233, 207);
            this.EditDescriptionTextBox.TabIndex = 0;
            this.EditDescriptionTextBox.Text = "";
            // 
            // EditNameIncidentTextBox
            // 
            this.EditNameIncidentTextBox.Location = new System.Drawing.Point(46, 25);
            this.EditNameIncidentTextBox.Name = "EditNameIncidentTextBox";
            this.EditNameIncidentTextBox.Size = new System.Drawing.Size(233, 20);
            this.EditNameIncidentTextBox.TabIndex = 1;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(94, 316);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(113, 46);
            this.SaveButton.TabIndex = 2;
            this.SaveButton.Text = "Save Changes";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // numberIncLabel
            // 
            this.numberIncLabel.AutoSize = true;
            this.numberIncLabel.Location = new System.Drawing.Point(12, 9);
            this.numberIncLabel.Name = "numberIncLabel";
            this.numberIncLabel.Size = new System.Drawing.Size(83, 13);
            this.numberIncLabel.TabIndex = 3;
            this.numberIncLabel.Text = "numberIncLabel";
            // 
            // EditIncident
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(313, 374);
            this.Controls.Add(this.numberIncLabel);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.EditNameIncidentTextBox);
            this.Controls.Add(this.EditDescriptionTextBox);
            this.Name = "EditIncident";
            this.Text = "EditIncident";
            this.Load += new System.EventHandler(this.EditIncident_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox EditDescriptionTextBox;
        private System.Windows.Forms.TextBox EditNameIncidentTextBox;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Label numberIncLabel;
    }
}