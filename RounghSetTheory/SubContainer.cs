﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RounghSetTheory
{
   public class SubContainer
    {
        public string nameTypeIncident;
        public string nameIncident;
        public string description;
        public string solution;
        public string date;

        public float? alpha;
        public float? betta;

        public int? editIndex;

        public List<List<float>> matrix;

        public Dictionary<string, string> classes;

        public List<TypeIncident> incidents;


        public DataGridView incidentsTable;

        public DataGridView classesTable;

        public ComboBox typeIncidents;
        
        public void FillingFormsElement (DataGridView incidentsTable, DataGridView classesTable, ComboBox typeIncidents)
        {
            this.incidentsTable = incidentsTable;
            this.classesTable = classesTable;
            this.typeIncidents = typeIncidents;
        }


      


        /// <summary>
        /// заполненяет один инцидент
        /// </summary>
        /// <param name="nameTypeIncident">Имя типа инцидента</param>
        /// <param name="nameIncident">Имя инцидента</param>
        /// <param name="description">Описание инцидента</param>
        /// <param name="solution">Решение по инциденту</param>
        /// <param name="alpha">Альфа диапазона</param>
        /// <param name="betta">Бетта диапазона</param>
        /// <param name="matrix">Матрица измерений</param>
        /// <param name="classes">Словарь классов инцидента</param>
        public void Filling (string nameTypeIncident, string nameIncident, string description, string solution, float alpha, float betta, List<List<float>> matrix, Dictionary<string, string> classes)
        {
            this.nameTypeIncident = nameTypeIncident;
            this.nameIncident = nameIncident;
            this.description = description;
            this.solution = solution;
            this.alpha = alpha;
            this.betta = betta;
            this.matrix = matrix;
            this.classes = classes;
        
        }

        bool IsNull<T>(T link)
        {
            return (link == null) ? true : false;
        }


        public Incident GetIncident (string nameTypeIncident, string nameIncident)
        {
            if (!IsNull(incidents))
            {
                foreach (var value in incidents)
                {
                    if (value.NameTypeIncident == nameTypeIncident)
                    {
                        foreach (var inc in value.incidents)
                        {
                            if (inc.NameIncident == nameIncident)
                            {
                                
                                return inc;
                            }
                               
                        }
                       
                    }
                }

                MessageBox.Show("This Incident Not Found");
                return null;
            }

            MessageBox.Show("The XML file is empty, edtit file");
            return null;

        }

        public void SetIncident(string oldName, EditIncident.EditingData data)
        {

            foreach (var value in incidents)
            {
                if (value.NameTypeIncident == data.TypeIncidentName)
                {
                    foreach (var inc in value.incidents)
                    {
                        if (inc.NameIncident == oldName)
                        {
                            inc.NameIncident = data.Name;
                            inc.Description = data.Description;
                        }
                    }
                }
            }

        }

        public string GetDesription(string nameTypeIncident, string nameIncident)
        {
            foreach (var value in incidents)
            {
                if (value.NameTypeIncident == nameTypeIncident)
                {
                    foreach (var inc in value.incidents)
                    {
                        if (inc.NameIncident == nameIncident)
                            return inc.Description;
                    }
                }
            }

            return "Description not found";
        }




       public void AddIncident (string nameTypeInc, Incident newInc)
        {
            foreach (var value in incidents)
            {
                MessageBox.Show("EPTA");
                if (value.NameTypeIncident == nameTypeInc)
                {
                    
                    value.incidents.Add(newInc);
                }
            }
        }



        public Incident GetNewIncident(string nameTypeIncident, string nameIncident, string Description)
        {
            List<List<float>> tempList = new List<List<float>>();

            for (int i = 0; i < 3; i++)
            {
                tempList.Add(new List<float>());

                for (int j = 0; j < 3; j++)
                    tempList[i].Add(0);

            }

            Dictionary<string, string> tempDict = new Dictionary<string, string>
            {
                { "class1", "Class1" },
                { "class2", "Class2" },
                { "class3", "Class3" }
            };

            var filling = (nameIncident, Description, new float[] { 0, 1 }, tempList, tempDict, "no", "1.1.2018");

            Incident tempInc = new Incident(filling);

            return tempInc;
        }

    }
}
