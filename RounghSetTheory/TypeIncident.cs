﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace RounghSetTheory
{
    public class Incident
    {
        public string NameIncident { get;  set; }
        public string Description { get; set; }
        public float[] range = new float[2];
        public List<List<float>> Array = new List<List<float>>();
        public Dictionary<string, string> classes = new Dictionary<string, string>();
        public string date;

        private string solution;
        public string Solution
        {
            get { return solution ?? "no"; }

            set { solution = value; }
        }

        public Incident((string, string, float[], List<List<float>>, Dictionary<string, string>, string, string) incident)
        {
            NameIncident = incident.Item1;
            Description = incident.Item2;
            range = incident.Item3;
            Array = incident.Item4;
            classes = incident.Item5;
            Solution = incident.Item6;
            date = incident.Item7;
        }
      
    }

    public class TypeIncident
   {
       public string NameTypeIncident {get; set;}

       public List<Incident> incidents;

        public TypeIncident()
        {
            incidents = new List<Incident>();
        }


   }

    


}
