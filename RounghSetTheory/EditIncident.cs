﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RounghSetTheory
{
    public partial class EditIncident : Form
    {
        public MainEvents WriteData;
        bool isEdit;
        public MainEvents ISChange;


        public EditIncident()
        {
            InitializeComponent();

            numberIncLabel.Visible = false;

            WriteData = EditMethod;
            ISChange = AddMethod;
        }

        public class EditingData
        {
            public string Name { get; private set; }
            public string Description { get; private set; }
            public string TypeIncidentName { get; private set; }


            public EditingData(string name, string description, string nameTypeIncident)
            {
                Name = name;
                Description = description;
                TypeIncidentName = nameTypeIncident;
            }

        }


        void EditMethod(SubContainer helper)
        {
            EditNameIncidentTextBox.Text = helper.nameIncident;
            EditDescriptionTextBox.Text = helper.description;
            numberIncLabel.Text = helper.nameTypeIncident;
            isEdit = true;
        }

        void AddMethod (SubContainer helper)
        {
            isEdit = false;
            numberIncLabel.Text = helper.nameTypeIncident;

        }

        private void EditIncident_Load(object sender, EventArgs e)
        {
            
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {

            if (isEdit)
                Main.Edit(new EditingData(EditNameIncidentTextBox.Text, EditDescriptionTextBox.Text, numberIncLabel.Text));

            else
                Main.Add(new EditingData(EditNameIncidentTextBox.Text, EditDescriptionTextBox.Text, numberIncLabel.Text));

            this.Close();

        }
    }
}
