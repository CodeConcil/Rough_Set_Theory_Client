﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace RounghSetTheory
{
    class AddingInForm 
    {
        /// <summary>
        /// добавляет типы инцидентов в комбо бокс
        /// </summary>
        public MainEvents CallAddInComboBox = (SubContainer helper) => 
        {
           foreach (var value in helper.incidents)
                helper.typeIncidents.Items.Add(value.NameTypeIncident);

            helper.typeIncidents.SelectedItem = helper.typeIncidents.Items[0];
            
        };

        /// <summary>
        /// заполняет таблицу инцидентов для одного типа
        /// </summary>
        public MainEvents CallAddInDataGridInc = (SubContainer helper) =>
        {
       
            helper.incidentsTable.Rows.Clear();
          
            foreach (var value in helper.incidents)
                if (value.NameTypeIncident == helper.nameTypeIncident)
                    foreach (var inc in value.incidents)
                        helper.incidentsTable.Rows.Add(inc.NameIncident, inc.Solution);

            helper.incidentsTable.Rows[0].Selected = true;

        };


        /// <summary>
        /// заполняет таблицу классов инцидентов для одного инцидента
        /// </summary>
        public MainEvents CallAddInDataGridClass = (SubContainer helper) =>
        {
            helper.classesTable.Rows.Clear();
           
            Incident mainInc = helper.GetIncident(helper.nameTypeIncident, helper.nameIncident);
            foreach (var value in mainInc.classes.Keys)
            {
                MessageBox.Show(value);
                helper.classesTable.Rows.Add(value, mainInc.classes[value]);
            }

            
        };
        
    }
}
