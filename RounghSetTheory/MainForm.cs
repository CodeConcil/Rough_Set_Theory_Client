﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RounghSetTheory
{
    public delegate void MainEvents(SubContainer helper);
    public delegate void VoidEvents();
    public delegate void EditAdd(EditIncident.EditingData data);
  

    public partial class Main : Form
    {

        WorkInXML work = new WorkInXML();
        AddingInForm adding = new AddingInForm();
        SubContainer helper = new SubContainer();

        public static EditAdd Edit;
        public static EditAdd Add;

        public Main()
        {
            InitializeComponent();

            Edit = EditIncident;
            Add = AddIncident;

            helper.FillingFormsElement(IncidentsTable, classTable, TypeIncidentComboBox);

            work.CallLoadXML();

            helper.incidents = work.GetIncidentsList();


            adding.CallAddInComboBox(helper);           
        }

        private void EditButton_Click(object sender, EventArgs e)
        {

            EditIncident editIncident = new EditIncident();

            helper.nameTypeIncident = TypeIncidentComboBox.SelectedItem.ToString();
            helper.nameIncident = IncidentsTable.CurrentRow.Cells[0].Value.ToString();

            helper.description = helper.GetDesription(helper.nameTypeIncident, helper.nameIncident);

            editIncident.WriteData(helper);

            editIncident.Show();
                 
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            EditIncident editIncident = new EditIncident();

            helper.nameTypeIncident = TypeIncidentComboBox.SelectedItem.ToString();

            editIncident.ISChange(helper);
            editIncident.Show();
        }

        void AddIncident (EditIncident.EditingData data)
        {

            helper.nameTypeIncident = data.TypeIncidentName;
            helper.nameIncident = data.Name;
            helper.description = data.Description;

            MessageBox.Show(helper.nameTypeIncident);
                
            Incident newInc = helper.GetNewIncident(helper.nameTypeIncident, helper.nameIncident, helper.description);

            helper.classes = newInc.classes;
            helper.alpha = newInc.range[0];
            helper.betta = newInc.range[1];
            helper.matrix = newInc.Array;
            helper.solution = newInc.Solution;

            helper.AddIncident(helper.nameTypeIncident, newInc);

            MessageBox.Show(helper.incidents[0].incidents[helper.incidents[0].incidents.Count-1].NameIncident);

            adding.CallAddInDataGridInc(helper);
            work.CallAddInXML(helper);
        }
      


        private void EditIncident (EditIncident.EditingData data)
        {


            helper.SetIncident(helper.nameIncident, data);

            helper.nameTypeIncident = data.TypeIncidentName;
            helper.nameIncident = data.Name;
            helper.description = data.Description;
            helper.editIndex = IncidentsTable.CurrentRow.Index;

            adding.CallAddInDataGridInc(helper);

            work.CallEditXML(helper);
        }

        private void TypeIncidentComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            helper.nameTypeIncident = TypeIncidentComboBox.SelectedItem.ToString();
            adding.CallAddInDataGridInc(helper);
        }

        private void IncidentsTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            helper.nameTypeIncident = TypeIncidentComboBox.SelectedItem.ToString();
            helper.nameIncident = IncidentsTable.CurrentRow.Cells[0].Value.ToString();

            adding.CallAddInDataGridClass(helper);
        }


        private void IncidentsTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Main_Load(object sender, EventArgs e)
        {

        }
    }
}
