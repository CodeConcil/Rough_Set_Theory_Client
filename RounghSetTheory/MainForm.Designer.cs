﻿namespace RounghSetTheory
{
    partial class Main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddButton = new System.Windows.Forms.Button();
            this.EditButton = new System.Windows.Forms.Button();
            this.IncidentsTable = new System.Windows.Forms.DataGridView();
            this.NameIncident = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Solution = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiagrammPanel = new System.Windows.Forms.Panel();
            this.TypeIncidentComboBox = new System.Windows.Forms.ComboBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.classTable = new System.Windows.Forms.DataGridView();
            this.ClassColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SolutionColimn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentsTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classTable)).BeginInit();
            this.SuspendLayout();
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(1116, 463);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(95, 43);
            this.AddButton.TabIndex = 0;
            this.AddButton.Text = "Add Incident";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // EditButton
            // 
            this.EditButton.Location = new System.Drawing.Point(998, 463);
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(95, 43);
            this.EditButton.TabIndex = 1;
            this.EditButton.Text = "Edit Incident";
            this.EditButton.UseVisualStyleBackColor = true;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // IncidentsTable
            // 
            this.IncidentsTable.AllowUserToOrderColumns = true;
            this.IncidentsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.IncidentsTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameIncident,
            this.Solution});
            this.IncidentsTable.Location = new System.Drawing.Point(12, 49);
            this.IncidentsTable.Name = "IncidentsTable";
            this.IncidentsTable.Size = new System.Drawing.Size(395, 187);
            this.IncidentsTable.TabIndex = 2;
            this.IncidentsTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.IncidentsTable_CellClick);
            this.IncidentsTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.IncidentsTable_CellContentClick);
            // 
            // NameIncident
            // 
            this.NameIncident.HeaderText = "Name Incident";
            this.NameIncident.Name = "NameIncident";
            this.NameIncident.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NameIncident.Width = 300;
            // 
            // Solution
            // 
            this.Solution.HeaderText = "Solution";
            this.Solution.Name = "Solution";
            this.Solution.Width = 50;
            // 
            // DiagrammPanel
            // 
            this.DiagrammPanel.Location = new System.Drawing.Point(413, 12);
            this.DiagrammPanel.Name = "DiagrammPanel";
            this.DiagrammPanel.Size = new System.Drawing.Size(798, 445);
            this.DiagrammPanel.TabIndex = 3;
            // 
            // TypeIncidentComboBox
            // 
            this.TypeIncidentComboBox.FormattingEnabled = true;
            this.TypeIncidentComboBox.Location = new System.Drawing.Point(12, 12);
            this.TypeIncidentComboBox.Name = "TypeIncidentComboBox";
            this.TypeIncidentComboBox.Size = new System.Drawing.Size(395, 21);
            this.TypeIncidentComboBox.TabIndex = 4;
            this.TypeIncidentComboBox.SelectedIndexChanged += new System.EventHandler(this.TypeIncidentComboBox_SelectedIndexChanged);
            // 
            // classTable
            // 
            this.classTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.classTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ClassColumn,
            this.SolutionColimn});
            this.classTable.Location = new System.Drawing.Point(12, 261);
            this.classTable.Name = "classTable";
            this.classTable.Size = new System.Drawing.Size(395, 109);
            this.classTable.TabIndex = 5;
            // 
            // ClassColumn
            // 
            this.ClassColumn.HeaderText = "Name Class";
            this.ClassColumn.Name = "ClassColumn";
            this.ClassColumn.Width = 300;
            // 
            // SolutionColimn
            // 
            this.SolutionColimn.HeaderText = "Solution";
            this.SolutionColimn.Name = "SolutionColimn";
            this.SolutionColimn.Width = 50;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1223, 518);
            this.Controls.Add(this.classTable);
            this.Controls.Add(this.TypeIncidentComboBox);
            this.Controls.Add(this.DiagrammPanel);
            this.Controls.Add(this.IncidentsTable);
            this.Controls.Add(this.EditButton);
            this.Controls.Add(this.AddButton);
            this.Name = "Main";
            this.Text = "Rough Set Theory";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.IncidentsTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button EditButton;
        private System.Windows.Forms.DataGridView IncidentsTable;
        private System.Windows.Forms.Panel DiagrammPanel;
        protected System.Windows.Forms.ComboBox TypeIncidentComboBox;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DataGridView classTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClassColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SolutionColimn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameIncident;
        private System.Windows.Forms.DataGridViewTextBoxColumn Solution;
    }
}

